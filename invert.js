newObj = {};
function invert(obj) {
  for (key in obj) {
    newObj[obj[key]] = key;
  }
  return newObj;
}

module.exports = invert;
