let mapObj = {};
function mapObject(obj, cb) {
  for (key in obj) {
    cb(key, obj[key]);
  }
  return mapObj;
}

function cb(key, value) {
  if (typeof value === "number") {
    mapObj[key] = value * 2;
  } else if (typeof value === "string") {
    mapObj[key] = value.toUpperCase();
  } else {
    mapObj[key] = null;
  }
}

module.exports = { mapObject, cb };
