const values = require("../values.js");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

try {
  console.log(values(testObject));
} catch (error) {
  console.log("There is some error in code");
}
