const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions
const defaultProps = { name: "Johnny", id: 1 };
const defaults = require("../defaults.js");

try {
  console.log(defaults(testObject, defaultProps));
} catch (error) {
  console.log("There is some error in code");
}
