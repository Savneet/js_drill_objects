const invert = require("../invert.js");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions

try {
  console.log(invert(testObject));
} catch (error) {
  console.log("There is some error in code");
}
