let pairArray = [];

function pairs(obj) {
  for (key in obj) {
    pairArray.push([key, obj[key]]);
  }
  return pairArray;
}

module.exports = pairs;
