let keysArray = [];
function keys(obj) {
  for (let key in obj) {
    keysArray.push(key);
  }
  return keysArray;
}

module.exports = keys;
