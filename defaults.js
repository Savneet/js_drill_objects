const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" }; // use this object to test your functions
const defaultProps = { name: "Johnny", id: 1 };

// const testObject={flavor: "chocolate"};
// const defaultProps={flavor: "vanilla", sprinkles: "lots"};

function defaults(obj, defaultProps) {
  for (key in defaultProps) {
    if (obj[key] === undefined) {
      obj[key] = defaultProps[key];
    }
  }
  return obj;
}
module.exports = defaults;
